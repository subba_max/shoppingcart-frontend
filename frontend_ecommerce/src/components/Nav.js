// import './App.css'
import React from 'react';
import styled from "styled-components";
// import Login from './components/Login'
import { BrowserRouter as Router,Switch,Route,Link } from 'react-router-dom'
// import Products from '../components/Products'
// import Login from '../components/Login'
import { ButtonContainer } from "../Button";

const Nav = () => {
    // const navStyle = {
    //     color:'white'
    // };
    const NavWrapper = styled.nav`
  background: var(--mainBlue);
  .nav-link {
    color: var(--mainWhite) !important;
    font-size: 1.3rem;
    text-transform: capitalize;
  }
`;
    
    return (
        <NavWrapper className="navbar navbar-expand-sm navbar-dark px-sm-5">
        {/* 
https://www.iconfinder.com/icons/1243689/call_phone_icon
Creative Commons (Attribution 3.0 Unported);
https://www.iconfinder.com/Makoto_msk */}
        <Link to="/">
          {/* <img src={logo} alt="store" className="navbar-brand" /> */}
        </Link>
        <ul className="navbar-nav align-items-center">
          <li className="nav-item ml-5">
            <Link to="/" className="nav-link">
              products
            </Link>
          </li>
          
        </ul>
        <Link to="/cart" className="ml-auto">
          <ButtonContainer>
            <span className="mr-2">
              <i className="fas fa-cart-plus" />
            </span>
            my cart
          </ButtonContainer>
         
        </Link>
        <Link to="/login" className="ml-auto">
          <ButtonContainer>
            <span className="mr-2">
              {/* <i className="fas fa-cart-plus" /> */}
            </span>
           Login
          </ButtonContainer>
         
        </Link>
      </NavWrapper>
        
    //    <nav className=" bg-primary">
    //     <ul className=" btn btn-group d-flex col-md-3" >
    //     <Link to='/products' className=" btn btn-success c"> products </Link>{'  '}
       
    //     <Link to='/login' className=" btn btn-success">  Login</Link>{ '  '}
    //    <Link to="/cart" className=" btn btn-success"> Cart</Link>{' '}

    //     </ul>
    //     </nav>
        
       
    );
}
export default Nav;