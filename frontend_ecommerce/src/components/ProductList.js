import React from 'react';

const  ProductList = (props) =>
{
    return (
        <div>
        { props.products && props.products.map( product  =>{
            return <div>{product.title}</div>
        })}
            
       </div>            
            
    )
}

export default ProductList;