import logo from './logo.svg';
import './App.css';
import Nav from './components/Nav';
import Products from './Products';
import {BrowserRouter as Router ,Switch ,Route } from 'react-router-dom';
import Login from './components/Login';
import Cart from './components/Cart';
import "bootstrap/dist/css/bootstrap.min.css";
import Default from './components/Default'
function App() {
  return (
    
    <div className="App">
    
     {/* <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" /> 
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> 
     */}
   <Router>
   <Nav />
   <Switch>
   <Route path="/"  exact component={Products}></Route>
     <Route path="/login" component={Login}></Route>
     <Route path="/cart" component={Cart}></Route>
     <Route  component={Default}></Route>
   </Switch>
   </Router>
     
    
    </div>
  );
}

export default App;
